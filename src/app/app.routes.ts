import { Routes } from '@angular/router';
import { authGuard } from '@lib/guards';

export const routes: Routes = [
    {
        path: 'login',
        loadChildren: async () => (await import('@pages/login')).routes,
        canMatch: [authGuard({ requiresAuthentication: false })],
    },
    {
        path: 'register',
        loadChildren: async () => (await import('@pages/login/register')).routes,
        canMatch: [authGuard({ requiresAuthentication: false })],
    },
    {
        path: 'recovery',
        loadChildren: async () => (await import('@pages/login/recovery')).routes,
        canMatch: [authGuard({ requiresAuthentication: false })],
    },
    {
        path: 'change-password',
        loadChildren: async () => (await import('@pages/login/change-password')).routes,
        canMatch: [authGuard({ requiresAuthentication: false })],
    },
    {
        path: '',
        loadChildren: async () => (await import('@pages/home')).routes,
        canMatch: [authGuard()],
    },
    {
        path: 'users/:username',
        loadChildren: async () => (await import('@pages/user')).routes,
        canMatch: [authGuard()],
    },
    {
        path: 'settings',
        loadChildren: async () => (await import('@pages/settings')).routes,
        canMatch: [authGuard()],
    },
    {
        path: 'files',
        loadChildren: async () => (await import('@pages/util/files')).routes,
        canMatch: [authGuard()],
    },
    {
        path: 'cuestionario',
        loadChildren: async () => (await import('@pages/util/survey')).routes,
        canMatch: [authGuard()],
    },
    {
        path: 'busqueda-usuarios',
        loadChildren: async () => (await import('@pages/util/busqueda-usuarios')).routes,
        canMatch: [authGuard()],
    },
    {
        path: '**',
        loadComponent: async () => (await import('@pages/screens/not-found/not-found.component')).NotFoundComponent,
    },
];
