import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Router } from '@angular/router';

@Component({
  selector: 'vex-formularios-seccion',
  templateUrl: './formularios-seccion.component.html',
  styleUrls: ['./formularios-seccion.component.scss']
})
export class FormulariosSeccionComponent implements OnInit {

  @Input() secciones: any;
  @Input() seccion: any;
  @Input() index: any;
  @Input() survey: any;
  @Output() addSection: any = new EventEmitter<any>();
  @Output() addQuestion: any = new EventEmitter<any>();
  @Output() changeSelect: any = new EventEmitter<any>();
  @Output() changeOther: any = new EventEmitter<any>();
  @Output() changeSelectLista: any = new EventEmitter<any>();
  @Output() changeData: any = new EventEmitter<any>();
  @Output() changeName: any = new EventEmitter<any>();
  @Output() changeRequired: any = new EventEmitter<any>();
  @Output() changeFronteraUser: any = new EventEmitter<any>();
  @Output() changeCheckDate: any = new EventEmitter<any>();
  @Output() changeValor: any = new EventEmitter<any>();
  @Output() duplicate: any = new EventEmitter<any>();
  @Output() changeAnswer: any = new EventEmitter<any>();
  @Output() changeSize: any = new EventEmitter<any>();
  @Output() delete: any = new EventEmitter<any>();
  @Output() changeConditionSelection: any = new EventEmitter<any>();
  condition1
  condition2
  typeQuestion = 0;
  typeQuestions = [
    // {
    //   id: 9,
    //   name: "Lista"
    // },
    {
      id: 1,
      name: "Respuesta unica"
    },
    {
      id: 2,
      name: "Multiple"
    },
    {
      id: 3,
      name: "Corta"
    },
    {
      id: 4,
      name: "Párrafo"
    },
    {
      id: 5,
      name: "Númerico"
    },
    {
      id: 6,
      name: "Email"
    },
    {
      id: 7,
      name: "Archivo"
    },
    {
      id: 8,
      name: "Fecha"
    },
    {
      id: 9,
      name: "Respuesta unica con valor"
    },
    {
      id: 10,
      name: "Muestra"
    }
  ];

  typeCar : any = [
    {
      label:'Publico',
      value : 2
    },
    {
      label:'Particular',
      value : 5
    }
  ]
  seccionForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private cdRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.seccionForm = this.fb.group({
      message: new FormControl('', [Validators.required]),
      typeQuestion: new FormControl('', [Validators.required]),
    });

    console.log({'questions':this.seccion.questions})
  }

  addOptions(i:any){
    this.seccion.questions[i].optionsConditionDate = [{
      text: "",
      days: 1
    }];
  }

  addOptionConditionDate(ind: any, j :any){
    // console.log(data);
    this.seccion.questions[ind].optionsConditionDate.push({
      text: "",
      days: 1
    });
  }

  changeSelectConditionDateHandle(ind: any){
    this.changeSelect.emit(ind);
  }

  deleteOptionConditionDate(ind: any){
    this.seccion.questions[ind].pop();
  }




  changeSelectListaHandle(option,index){
    let data={
      seccion: this.index,
      question: index,
      option
    }
    this.changeSelectLista.emit(data);
  }
  changeSelectHandle(option,index){
    let data={
      seccion: this.index,
      question: index,
      option
    }
    this.changeSelect.emit(data);
  }
  changeOtherHandle(index){
    let data = {
      seccion: this.index,
      question: index
    }
    this.changeOther.emit(data);
  }
  duplicateHandle(index){
    let data={
      seccion: this.index,
      question: index
    }
    this.duplicate.emit(data);
  }
  deleteHandle(index){
    let data={
      seccion: this.index,
      question: index
    }
    this.delete.emit(data);
  }

  changeType(e,indexQuestion){
    let data={
      seccion: this.index,
      question: indexQuestion,
      select: e
    }
    this.changeData.emit(data);
  }

  changeNameHandle(e,indexQuestion){
    let data={
      seccion: this.index,
      question: indexQuestion,
      value: e.target.value
    }
    this.changeName.emit(data);
  }
  changeAnswerHandle(text, indexQuestion){
    let data = {
      seccion: this.index,
      question: indexQuestion,
      value: text
    }
    console.log(text);
    this.changeAnswer.emit(data);
  }
  changeSizeHandle(text, indexQuestion){
    let data = {
      seccion: this.index,
      question: indexQuestion,
      value: text
    }
    console.log(data);
    this.changeSize.emit(data);
  }
  changeRequiredHandle(e,indexQuestion){
    let data={
      seccion: this.index,
      question: indexQuestion,
      value: e.target.value
    }
    this.changeRequired.emit(data);
  }
  changeFronteraUserHandle(e,indexQuestion){
    let data={
      seccion: this.index,
      question: indexQuestion,
      value: e.target.value
    }
    this.changeFronteraUser.emit(data);
  }

  changeCheckDateHandle(data) {
    data.seccion=this.index;
    this.changeCheckDate.emit(data);
  }

  changeValorHandle(e,indexQuestion){
    let data={
      seccion: this.index,
      question: indexQuestion,
      value: e.target.value
    }
    this.changeValor.emit(data);
  }

  addSectionHandle(){
    this.addSection.emit(this.index);
  }
  addQuestionHandle(){
    this.addQuestion.emit(this.index);
  }
  disableQuestions(k, i, j, question2){
    if (question2.type == 8){
      return true;
    }
    if(k>this.index){
      return true;
    }
    if(k==this.index){
      if(j>=i){
        return true;
      }
    }
  }

  compareFn(role1: any, role2: any) {
    return role1 && role2 ? role1.id === role2.id : role1 === role2;
  }
  compareFn2(role1: any, role2: any) {
    return role1 && role2 ? (role1[0] === role2[0] && role1[1] === role2[1]) : role1 === role2;
  }

  selectConditionQuestion(e,index){
    let select=0;
    let section=0;
    let cont=0;
    let ifChoices =[];
    this.secciones.map((it,i)=>{
      it.questions.map((jt,j)=>{
        if (jt.name == e.source.triggerValue){
          select=cont;
          section=i;
          ifChoices=jt.choices;
        }
        cont++;
      })
    });
    let data={
      section,
      question: index,
      select,
      ifChoices: JSON.parse(JSON.stringify(ifChoices))
    }
    console.log(data);
    this.changeConditionSelection.emit(data);
  }
}
