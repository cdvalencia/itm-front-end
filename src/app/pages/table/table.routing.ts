import { Routes, RouterModule } from "@angular/router";
import { RequestComponent } from "./request.component";
import { NewRequestComponent } from "./new-request/new-request.component";

const routes: Routes = [
  {
    path: "",
    component: RequestComponent,
  },{
    path: "new-request",
    component: NewRequestComponent,
  },
];

export const RequestRoutes = RouterModule.forChild(routes);
