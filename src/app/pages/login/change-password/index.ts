import { Routes } from '@angular/router';

export const routes: Routes = [
    {
        path: '',
        title: 'Change Password',
        loadComponent: async () => (await import('./change-password.component')).ChangePasswordComponent,
    },
];
