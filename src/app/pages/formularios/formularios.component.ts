import { Component, OnInit  } from '@angular/core';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { FormulariosService } from '../../../app/services/formularios/formularios.service';
import { SecurityService } from 'app/services/security/security.service';
import { PageEvent } from "@angular/material/paginator";
import { MatDialog } from '@angular/material/dialog';
import { Formularios } from "app/models/formularios/formularios.model";
import { NewFormComponent } from './new-form/new-form.component';

import {
  ActionsModel,
  EventTableModel,
  TableColumnsModel,
} from "app/shared/table/table.model";
@Component({
  selector: 'vex-formularios',
  templateUrl: './formularios.component.html',
  styleUrls: ['./formularios.component.scss']
})
export class FormulariosComponent implements OnInit {

  data: any[];
  columns: TableColumnsModel[] = [
    {
      label: "Id",
      property: "request.id",
      type: "text",
      visible: true,
    },
    {
      label: "Nombre",
      property: "request.name",
      type: "text",
      visible: true,
    },
    {
      label: "Activo",
      property: "request.active",
      type: "toggle",
      visible: true,
    },
    { label: "Opciones", property: "actions", type: "actions", visible: true },
  ];
  menuActions: ActionsModel[] = [
    {
      text: "Visualizar Componente",
      icon: "remove_red_eye",
      property: "preview",
      color: "gray",
    },
    {
      text: "Editar Componente",
      icon: "edit",
      property: "editar",
      color: "gray",
    }
  ];
  step=1;
  pageSize = 5;
  page = 1;
  totalItems;
  searchText: string = "";
  id: any= null;
  dataAll=[];
  constructor(
    private router: Router,
    private formulariosService: FormulariosService,
    private _securityService: SecurityService,
    private _dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getData();
  }


  search(text) {
    this.searchText=text;
    if (this.searchText == "") {
      this.data = this.dataAll;
      return;
    }
    console.log(this.searchText);

    this.data = this.data.filter((it, i) => {
      console.log(it);
      return it.request.name.toLowerCase().indexOf(this.searchText.toLowerCase()) != -1;
    });
  }
  getData(page?) {
    console.log(this.totalItems)
    this.formulariosService
      .getFormularios(
      )
      // .getFormulariosFind(
      //   {
      //     page: page || this.page,
      //     pageSize: this.pageSize,
      //     searchText: this.searchText,
      //   }
      // )
      .subscribe((data: any) => {
        this.data = data.data.map((formularios) => new Formularios(formularios));
        this.dataAll = this.data;

        // this.totalItems = data.pagination.totalItems;
      });
  }
  nextPage(page: PageEvent) {
    this.pageSize = page.pageSize;

    this.getData(page.pageIndex);
  }

  regresar() {
    this.step=1;
    this.getData();
  }

  crearFormulario() {
    this.step=2;
    this.id=null;
    // const dialogRef = this._dialog.open(NewFormComponent, {
    //   width: '95%',
    //   height: 'auto',
    // });
    // dialogRef.afterClosed().subscribe((data) => {
    //   if (data) {
    //   }
    // });
  }

  activeChange(center) {
    this.formulariosService.activeForm(center.id, center.active).subscribe((rt) => {
      if (rt["status"]) {
        swal.fire("El Centro ha sido actualizado!", "", "success");
      } else {
        swal.fire("Ha ocurrido un error, intente nuevamente.", "", "error");
      }
    });
  }

  opciones(datos: EventTableModel<any, "ver" | "editar" | "request.active" | "preview">) {
    console.log(datos);
    if (datos.event === "request.active") {
      this.activeChange(datos.data["request"]);
    }
    if (datos.event === "editar") {
      console.log(datos);
      //Acción a realizar
      this.step=2;
      this.id=datos.data.request.id;
    }
    if (datos.event === "preview") {
      console.log(datos);
      //Acción a realizar
      this.id=datos.data.request.id;
      window.open('/formularios/survey?id='+this.id, '_blank');
    }
  }
}
