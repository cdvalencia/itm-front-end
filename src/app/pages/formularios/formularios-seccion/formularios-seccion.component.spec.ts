import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulariosSeccionComponent } from './formularios-seccion.component';

describe('FormulariosSeccionComponent', () => {
  let component: FormulariosSeccionComponent;
  let fixture: ComponentFixture<FormulariosSeccionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormulariosSeccionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormulariosSeccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
