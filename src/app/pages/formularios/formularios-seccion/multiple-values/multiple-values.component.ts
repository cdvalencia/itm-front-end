import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'vex-multiple-values',
  templateUrl: './multiple-values.component.html',
  styleUrls: ['./multiple-values.component.scss']
})
export class MultipleValuesComponent implements OnInit {

  
  @Input() options: any; 
  @Input() survey: any;  
  @Input() other: any;  
  @Output() changeOther: any = new EventEmitter<any>(); 
  @Output() changeSelect: any = new EventEmitter<any>(); 

  options2=[]
 
  constructor() { }

  ngOnInit(): void {
    this.options2 = this.options;
  }

  addOption(ind: any){
    // console.log(data);
    this.options.push({
      text: "",
      check: false
    });
  }
  
  changeSelectHanlde(ind: any){
    this.changeSelect.emit(ind);
  } 
  
  deleteOption(ind: any){
    this.options.pop();
  }  

}
