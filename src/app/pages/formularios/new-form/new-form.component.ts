import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormulariosService } from '../../../../app/services/formularios/formularios.service';

import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { log } from 'console';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormulariosComponent } from '../formularios.component';

@Component({
  selector: 'vex-new-form',
  templateUrl: './new-form.component.html',
  styleUrls: ['./new-form.component.scss']
})
export class NewFormComponent implements OnInit {

  @Input() id: any;
  @Output() regresar: any = new EventEmitter<any>();

  secciones: any = [
    {
      questions: [
        {
          name: "",
          type: 0,
          required: false,
          valor: 0,
          size: 20,
          correctAnswer: ""
        }
      ]
    },
  ]

  type = 2;
  types: any[] = [
    {
      id: 1,
      name: "Cuestionario"
    },
    {
      id: 2,
      name: "Encuesta"
    }
  ];
  uid = undefined;
  survey = {
    "name": "",
    "description": "",
    "percentage": 0,
    "uid": "",
    "user": "",
    "type": 2,
    "showCompletedPage": false,
    "completedHtml": "",
    "pageNextText": "Siguiente",
    "pagePrevText": "Anterior",
    "completeText": "Finalizar",
    "showProgressBar": "top",
    "logo": "",
    "pages": []
  }
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formulariosService: FormulariosService,
    // public dialogRef: MatDialogRef<FormulariosComponent>

  ) {}

  ngOnInit(): void {
    if (this.id) {
      this.formulariosService.getFormulario(this.id).subscribe(
        rt => {
          console.log(rt);
          this.survey = rt.data.custom;
          this.survey.name = rt.data.name;
          this.survey.description = rt.data.description;
          if(!this.survey.pages){
            this.survey.pages=[];
          }else{
            this.secciones=[];
          }
          console.log(this.survey.pages);
          this.survey.showCompletedPage = false;
          this.initSurvey();
        }
      )
    } else {
      // this.survey = localStorage.getItem("survey"+this.id) ? JSON.parse(localStorage.getItem("survey"+this.id)) : {};
      // this.survey = {};
      // this.uid = this.route.snapshot.url[1].path;
      this.survey.type = 1;
      // if (this.survey.pages) {
      //   this.secciones = [

      //   ];
      // } else {
        this.secciones = [
          {
            colapsed: true,
            name: "Seccion 1",
            questions: [
              {
                name: "",
                type: 0,
                required: false,
                valor: 0,
                correctAnswer: "",
                accept: [],
                maxSize: "",
                size: 20,
                choices: [
                  {
                    text: "",
                    check: false
                  }
                ],
                typesFiles: [
                  {
                    id: 1,
                    name: "Hoja de calculo",
                    ext: ".xls,.xlsx"
                  },
                  {
                    id: 2,
                    name: "PDF",
                    ext: ".pdf"
                  },
                  {
                    id: 3,
                    name: "Documento",
                    ext: ".doc,.docs"
                  },
                  {
                    id: 4,
                    name: "Video",
                    ext: "video/*"
                  },
                  {
                    id: 5,
                    name: "Presentación",
                    ext: ".ppt,.pptx"
                  },
                  {
                    id: 6,
                    name: "Imagen",
                    ext: "image/*"
                  },
                ],
              }
            ]
          },
        ];
      // }
      this.initSurvey();
    }
  }


  initSurvey() {
    console.log(this.survey);
    if (this.survey.pages) {
      this.survey.pages.map((it, i) => {
        let page: any = {
          name: (it.name)?it.name:"Seccion " + i,
          questions: [],
          colapsed: true,
        }
        if (it.if != null && it.if != undefined) {
          page.if = it.if;
          // page.if = 21;
          page.if2 = it.if2;
          page.conditions = true;
        }
        let elements = [];
        it.elements.map((jt, j) => {

          let newElement: any = {};
          if (jt.type == "checkbox" || jt.type == "radiogroup") {
            newElement = {
              "type": 1,
              "name": jt.name,
              "visible": true,
              "title": jt.name,
              "valor": jt.valor,
              "required": (jt.isRequired),
              "fronteraUser": (jt.fronteraUser),
              "correctAnswer": jt.correctAnswer,
            }
          }
          if (jt.type == "checkbox") {
            newElement.type=2;
          }
          if ((jt.type == "checkbox" || jt.type == "radiogroup" || jt.type == "dropdown") && jt.lista) {
            newElement = {
              "type": 9,
              "name": jt.name,
              "visible": true,
              "title": jt.name,
              "valor": jt.valor,
              "required": (jt.isRequired),
              "fronteraUser": (jt.fronteraUser),
              "lista": jt.lista,
            }
          }
          if (jt.type == "text") {
            newElement = {
              "type": 3,
              "name": jt.name,
              "visible": true,
              "title": jt.name,
              "valor": jt.valor,
              "required": (jt.isRequired),
              "fronteraUser": (jt.fronteraUser),
              "checkDate": (jt.checkDate),
              "correctAnswer": jt.correctAnswer,
              "sizeMax": jt.sizeMax,
              "validators": jt.validators,
            }
            // console.log(jt.validators);
            // console.log((jt.validators && jt.validators[0] && jt.validators[0].type && jt.validators[0].type == "numeric"));
            if (jt.validators && jt.validators[0] && jt.validators[0].type && jt.validators[0].type == "numeric") {
              newElement.type = 5;
            }
            if (jt.validators && jt.validators[0] && jt.validators[0].type && jt.validators[0].type == "email") {
              newElement.type = 6;
            }
            if (jt.validators && jt.validators[0] && jt.validators[0].type && jt.validators[0].type == "date") {
              newElement.type = 8;
            }
          }
          if (jt.type == "comment") {
            newElement = {
              "type": 4,
              "name": jt.name,
              "visible": true,
              "title": jt.name,
              "valor": jt.valor,
              "required": (jt.isRequired),
              "fronteraUser": (jt.fronteraUser),
              "sizeMax": jt.sizeMax,
              "correctAnswer": jt.correctAnswer,
            }
          }

          if (jt.type == "datepicker") {
            console.log("AQUI", jt.type)
            newElement = {
              "type": 8,
              "inputType": jt.inputType,
              "name": jt.name,
              "visible": true,
              "title": jt.name,
              "valor": jt.valor,
              "required": (jt.isRequired),
              "fronteraUser": (jt.fronteraUser),
              "checkDate": (jt.checkDate),
              "sizeMax": jt.sizeMax,
              "correctAnswer": jt.correctAnswer,
            }
          }

          if (jt.type == "file") {
            newElement = {
              "type": 7,
              "name": jt.name,
              "visible": true,
              "title": jt.name,
              "valor": jt.valor,
              "required": (jt.isRequired),
              "fronteraUser": (jt.fronteraUser),
              "accept": jt.accept,
              "maxSize": jt.maxSize,
              "typesFiles": jt.typesFiles,
            }
          }
          if (jt.type == "html") {
            newElement = {
              "type": 10,
              "name": jt.name,
              "question": jt.question,
              "image": jt.image,
              "visible": true,
              "html": jt.html,
              "title": jt.name,
              "valor": jt.valor,
              "required": (jt.isRequired),
              "fronteraUser": (jt.fronteraUser),
              "accept": jt.accept,
              "maxSize": jt.maxSize,
              "typesFiles": jt.typesFiles,
            }
          }
          if (jt.if != null && jt.if != undefined) {
            newElement.if = jt.if;
            newElement.if2 = jt.if2;
            newElement.conditions = true;
          }
          newElement.conditionGlobalText = jt.conditionGlobalText;
          if (jt.conditionGlobal != null && jt.conditionGlobal != undefined) {
            newElement.conditionGlobal = jt.conditionGlobal;
            newElement.conditionGlobalToggle = true;
          }
          if (jt.uid) {
            newElement.uid = jt.uid;
          }
          newElement.choices = (jt.choices2) ? jt.choices2 : jt.choices;
          elements.push(newElement);
        });
        page.questions = elements;
        this.secciones.push(page);
      });
    }
    console.log(this.secciones);
  }

  cerrar() {
    // this.dialogRef.close(null);
  }

  back() {
    this.router.navigate(['/formularios'])
  }
  seeForm() {
    // this.router.navigate(['/formularios/survey']);
    // window.open('/formularios/survey', '_blank');
    this.saveSurvey();
  }

  addSection(ind) {
    this.secciones.splice(ind + 1, 0, {
      name: "Seccion " + Number(this.secciones.length+1),
      questions: [
        {
          type: 0,
          required: false,
          valor: 0,
          correctAnswer: "",
          size: 20,
          accept: [],
          maxSize: "",
          choices: [
            {
              text: "",
              check: false
            }
          ],
          typesFiles: [
            {
              id: 1,
              name: "Hoja de calculo",
              ext: ".xls,.xlsx"
            },
            {
              id: 2,
              name: "PDF",
              ext: ".pdf"
            },
            {
              id: 3,
              name: "Documento",
              ext: ".doc,.docs"
            },
            {
              id: 4,
              name: "Video",
              ext: "video/*"
            },
            {
              id: 5,
              name: "Presentación",
              ext: ".ppt,.pptx"
            },
            {
              id: 6,
              name: "Imagen",
              ext: "image/*"
            },
          ],
        }
      ]
    });
  }

  addQuestion(ind) {
    this.secciones[ind].questions.push(
      {
        name: "",
        type: 0,
        required: false,
        valor: 0,
        correctAnswer: "",
        size: 20,
        accept: [],
        maxSize: "",
        other: false,
        choices: [
          {
            text: "",
            check: false
          }
        ],
        typesFiles: [
          {
            id: 1,
            name: "Hoja de calculo",
            ext: ".xls,.xlsx"
          },
          {
            id: 2,
            name: "PDF",
            ext: ".pdf"
          },
          {
            id: 3,
            name: "Documento",
            ext: ".doc,.docs"
          },
          {
            id: 4,
            name: "Video",
            ext: "video/*"
          },
          {
            id: 5,
            name: "Presentación",
            ext: ".ppt,.pptx"
          },
          {
            id: 6,
            name: "Imagen",
            ext: "image/*"
          },
        ],
      }
    );
  }

  duplicate(data) {
    let newQuestion = JSON.parse(JSON.stringify(this.secciones[data.seccion].questions[data.question]));
    newQuestion.name = newQuestion.name+" copia";
    this.secciones[data.seccion].questions.splice(data.question + 1, 0, newQuestion);
  }
  delete(data) {
    this.secciones[data.seccion].questions.splice(data.question, 1);
    if (this.secciones[data.seccion].questions.length == 0) {
      this.secciones.splice(data.seccion, 1);
    }
  }

  changeData(data) {
    this.secciones[data.seccion].questions[data.question].type = data.select;
    if (data.select==7){
      this.secciones[data.seccion].questions[data.question].typesFiles= [
        {
          id: 1,
          name: "Hoja de calculo",
          ext: ".xls,.xlsx"
        },
        {
          id: 2,
          name: "PDF",
          ext: ".pdf"
        },
        {
          id: 3,
          name: "Documento",
          ext: ".doc,.docs"
        },
        {
          id: 4,
          name: "Video",
          ext: "video/*"
        },
        {
          id: 5,
          name: "Presentación",
          ext: ".ppt,.pptx"
        },
        {
          id: 6,
          name: "Imagen",
          ext: "image/*"
        },
      ]
    }
    if (data.select == 1 || data.select == 2){
      this.secciones[data.seccion].questions[data.question].choices= [
        {
          text: "",
          check: false
        }
      ]

      console.log(this.secciones[data.seccion].questions[data.question])
    }else{
      this.secciones[data.seccion].questions[data.question].conditionGlobalToggle =false;
    }
    console.log(data.select);
    if (data.select == 3 || data.select == 4){
      this.secciones[data.seccion].questions[data.question].size= 20;
    }

    if(data.select == 8){
      console.log("ELEGI FECHA")
      this.secciones[data.seccion].questions[data.question].choices= [
        {
          text: "",
          check: false
        }
      ]
      //this.secciones[data.seccion].questions[data.question].type = "datepicker"
      console.log(this.secciones[data.seccion].questions[data.question])
    }
  }

  changeName(data) {
    this.secciones[data.seccion].questions[data.question].name = data.value;
  }
  changeRequired(data) {
    console.log(data);
    this.secciones[data.seccion].questions[data.question].required = (this.secciones[data.seccion].questions[data.question].required) ? false : true;
  }
  changeFronteraUser(data) {
    this.secciones[data.seccion].questions[data.question].fronteraUser = (this.secciones[data.seccion].questions[data.question].fronteraUser) ? false : true;
  }
  changeCheckDate(data) {
    console.log(data);
    console.log(this.secciones[data.seccion].questions[data.question].checkDate);
    this.secciones[data.seccion].questions[data.question].checkDate = (this.secciones[data.seccion].questions[data.question].checkDate=="on") ? false : true;
  }
  changeValor(data) {
    this.secciones[data.seccion].questions[data.question].valor = data.value;
  }
  changeAnswer(data) {
    this.secciones[data.seccion].questions[data.question].correctAnswer = data.value;
  }
  changeSize(data) {
    this.secciones[data.seccion].questions[data.question].sizeMax = data.value;
  }
  changeSelect(data) {
    if (this.secciones[data.seccion].questions[data.question].type==1){
      this.secciones[data.seccion].questions[data.question].choices.map((it,i)=>{
        console.log(it);
        it.check=false;
      });
      this.secciones[data.seccion].questions[data.question].choices[data.option].check =true;
    }
  }
  changeOther(data) {
    this.secciones[data.seccion].questions[data.question].other = !this.secciones[data.seccion].questions[data.question].other;
  }
  changeSelectLista(data) {
    if (this.secciones[data.seccion].questions[data.question].type==9){
      this.secciones[data.seccion].questions[data.question].lista = data.option;
      console.log(this.secciones);
    }
  }
  changeConditionSelection(data) {
    // this.secciones[data.section].questions[data.question].ifIndex = data.select;
    // this.secciones[data.section].questions[data.question].ifChoices = data.ifChoices;
    // this.secciones[data.section].questions[data.question].ifSectionIndex = data.section;
  }

  saveSurvey() {
    let pages = [];
    let seemsName = false;
    if (!(this.survey.name && this.survey.name != "")) {
      swal.fire('Debe Asignarle un nombre al formulario.', '', 'warning');
      return;
    }
    if (!(this.survey.description && this.survey.description != "")) {
      swal.fire('Debe Asignarle una descripción al formulario.', '', 'warning');
      return;
    }
    if (!(this.survey.type && this.survey.type != 0)) {
      swal.fire('Debe Asignar un tipo de formulario.', '', 'warning');
      return;
    }
    this.secciones.map((it, i) => {
      let page: any = {
        name: (it.name)?it.name:"Seccion " + i,
        elements: []
      }
      if (it.if != null && it.if != undefined && it.conditions) {
        page.if = it.if;
        page.ifIndex = it.ifIndex;
        page.if2 = it.if2;
        // console.log(page);
        // console.log(it.questions[it.if]);
        console.log(this.secciones[it.if[0]]);
        console.log(this.secciones[it.if[0]].questions[it.if[0]]);
        if (this.secciones[it.if[0]].questions[it.if[1]].type == 2) {
          page.visibleIf = "{" + this.secciones[it.if[0]].questions[it.if[1]].name + "} = ['" + this.secciones[it.if[0]].questions[it.if[1]].choices[it.if2].text + "']"
        } else {
          page.visibleIf = "{" + this.secciones[it.if[0]].questions[it.if[1]].name + "} = '" + this.secciones[it.if[0]].questions[it.if[1]].choices[it.if2].text + "'"
        }
      }
      let elements = [];
      it.questions.map((jt, j) => {
        // console.log(jt);
        jt.name = (jt.name)?jt.name.trimLeft():"";
        jt.name = (jt.name)?jt.name.trimRight():"";
        let newElement: any = {
        };
        if (jt.type == 9) {
          newElement = {
            "type": "dropdown",
            "name": jt.name,
            "visible": true,
            "title": jt.name,
            "valor": jt.valor,
            "isRequired": (jt.required),
            "fronteraUser": (jt.fronteraUser),
            "correctAnswer": null,
            "lista" : jt.lista
          }
          if (jt.other) {
            newElement.hasOther = true;
            newElement.otherText = "Otro";
          }
        }
        if (jt.type == 1) {
          newElement = {
            "type": "radiogroup",
            "name": jt.name,
            "visible": true,
            "title": jt.name,
            "valor": jt.valor,
            "isRequired": (jt.required),
            "fronteraUser": (jt.fronteraUser),
            "correctAnswer": null,
            choices : jt.choices,
          }
          if (jt.other) {
            newElement.hasOther = true;
            newElement.otherText = "Otro";
          }
          if (this.survey.type == 1) {
            let correctAnswer = [];
            jt.choices.map((kt, k) => {
              if (kt["check"] == true) {
                correctAnswer.push(kt);
              }
            });
            newElement.correctAnswer = correctAnswer;
          }
        }
        if (jt.type == 2) {
          newElement = {
            "type": "checkbox",
            "name": jt.name,
            "visible": true,
            "title": jt.name,
            "valor": jt.valor,
            "isRequired": (jt.required),
            "fronteraUser": (jt.fronteraUser),
            "correctAnswer": null,
            choices: jt.choices,
          }
          if(jt.other){
            newElement.hasOther= true;
            newElement.otherText="Otro";
          }
          if (this.survey.type == 1) {
            let correctAnswer = [];
            jt.choices.map((kt, k) => {
              if (kt["check"] == true) {
                correctAnswer.push(kt);
              }
            });
            newElement.correctAnswer = correctAnswer;
          }
        }
        if (jt.type == 3) {
          newElement = {
            "type": "text",
            "name": jt.name,
            "visible": true,
            "title": jt.name,
            "valor": jt.valor,
            "isRequired": (jt.required),
            "fronteraUser": (jt.fronteraUser),
            "checkDate": (jt.checkDate),
            "correctAnswer": jt.correctAnswer,
          }
          if (jt.sizeMax && jt.sizeMax > 0 && jt.sizeMax != null && jt.sizeMax!=undefined){
            newElement.sizeMax= jt.sizeMax;
            newElement.validators= [
              {
                type: "regex",
                text: "Por favor debes digitar máximo " + jt.sizeMax + " caracteres. y sin caracteres especiales.",
                regex: "^[a-zA-Z0-9' ']{0," + jt.sizeMax + "}$"

              }
            ]
          }
        }
        if (jt.type == 4) {
          newElement = {
            "type": "comment",
            "name": jt.name,
            "visible": true,
            "title": jt.name,
            "valor": jt.valor,
            "isRequired": (jt.required),
            "fronteraUser": (jt.fronteraUser),
            "correctAnswer": jt.correctAnswer,
          }
          if (jt.sizeMax && jt.sizeMax > 0 && jt.sizeMax != null && jt.sizeMax != undefined) {
            newElement.sizeMax = jt.sizeMax;
            newElement.validators = [
              {
                type: "regex",
                text: "Por favor debes digitar máximo " + jt.sizeMax + " caracteres. y sin caracteres especiales.",
                regex: "^[a-zA-Z0-9' ']{0," + jt.sizeMax + "}$"

              }
            ]
          }
        }
        if (jt.type == 5) {
          newElement = {
            // "type": "number",
            "type": "text",
            "name": jt.name,
            "visible": true,
            "title": jt.name,
            "valor": jt.valor,
            "isRequired": (jt.required),
            "fronteraUser": (jt.fronteraUser),
            validators : [
              { "type": "numeric", "text": "La respuesta debe ser Númerica." },
            ]
          }
          if (jt.sizeMax && jt.sizeMax > 0 && jt.sizeMax != null && jt.sizeMax != undefined) {
            newElement.sizeMax = jt.sizeMax;
            newElement.validators.push(
              {
                type: "regex",
                text: "Por favor debes digitar máximo " + jt.sizeMax + " caracteres. y sin caracteres especiales.",
                regex: "^[a-zA-Z0-9' ']{0," + jt.sizeMax + "}$"
              }
            )
          }
        }
        if (jt.type == 6) {
          newElement = {
            // "type": "number",
            "type": "text",
            "name": jt.name,
            "visible": true,
            "title": jt.name,
            "valor": jt.valor,
            "isRequired": (jt.required),
            "fronteraUser": (jt.fronteraUser),
            "validators": [
              { "type": "email", "text": "Email no válido." }
            ],
          }
          if (jt.sizeMax && jt.sizeMax > 0 && jt.sizeMax != null && jt.sizeMax != undefined) {
            newElement.sizeMax = jt.sizeMax;
            newElement.validators.push(
              {
                type: "regex",
                text: "Por favor debes digitar máximo " + jt.sizeMax + " caracteres. y sin caracteres especiales.",
                regex: "^[a-zA-Z0-9' ']{0," + jt.sizeMax + "}$"

              }
            )
          }
        }

        if (jt.type == 8) {
          newElement = {
            // "type": "number",
            "type": "text",
            "inputType": "date",
            "autoComplete": "bdate",
            "name": jt.name,
            "visible": true,
            "title": jt.name,
            "valor": jt.valor,
            "isRequired": (jt.required),
            "fronteraUser": (jt.fronteraUser),
            "checkDate": (jt.checkDate),
            "validators": [
              { "type": "date", "text": "Fecha no válida." },
            ],
          }
        }

        if (jt.type == 7) {
          newElement = {
            "type": "file",
            "name": jt.name,
            "visible": true,
            "title": jt.name,
            "isRequired": (jt.required),
            "fronteraUser": (jt.fronteraUser),
            "allowMultiple": true,
            "accept": jt.accept,
            // "acceptedTypes": "image/*",
            "maxSize": jt.maxSize,
            "typesFiles": jt.typesFiles,
          }
          let acceptedTypes = "";
          newElement.typesFiles.map((it, i) => {
            if (i == 0) {
              acceptedTypes = it.ext;
            } else {
              acceptedTypes = acceptedTypes + "," + it.ext;
            }
          });
          newElement.acceptedTypes = acceptedTypes;
        }

        if (jt.type == 10) {
          let html="<div class='survey-muestra'><h4>"+jt.question+"</h4><figure><img src='"+jt.image+"'/></figure></div>";
          newElement = {
            // "type": "number",
            "type": "html",
            "html": html,
            "question": jt.question,
            "image": jt.image,
            "name": jt.name,
            "visible": true,
            "title": jt.name,
            "valor": jt.valor,
            "isRequired": (jt.required)
          }
        }

        // newElement.validators= [
        //   {
        //     type: "regex",
        //     text: "Please type exactly 5 digits",
        //     regex: "^\\d{5}$"
        //   }
        // ]

        // console.log(j, newElement ,newElement.choices);

        let newChoices=[];
        if (newElement.choices){
          newElement.choices2 = newElement.choices;
          newElement.choices2.map((kt, k) => {
            if (!(kt.text.length>0)) {
              kt.text="Opción "+(k+1);
            }
            newChoices.push(kt.text);
          });
          newElement.choices = newChoices;
        }
        elements.map((kt,k)=>{
          if(jt.name==kt.name){
            seemsName=true;
          }
        });
        console.log(jt);
        if (jt.if != null && jt.if != undefined && jt.conditions){
          newElement.if=jt.if;
          newElement.ifIndex = jt.ifIndex;
          newElement.if2=jt.if2;
          console.log(this.secciones[jt.if[0]].questions[jt.if[1]].type);
          if (this.secciones[jt.if[0]].questions[jt.if[1]].type ==2){
            newElement.visibleIf = "{" + this.secciones[jt.if[0]].questions[jt.if[1]].name + "} = ['" + this.secciones[jt.if[0]].questions[jt.if[1]].choices[jt.if2].text +"']"
          }else{
            if(jt.if && jt.if[1]!=undefined){
              newElement.visibleIf = "{" + this.secciones[jt.if[0]].questions[jt.if[1]].name + "} = '" + this.secciones[jt.if[0]].questions[jt.if[1]].choices[jt.if2].text +"'"
            }
          }
        }
        newElement.conditionGlobalText = jt.conditionGlobalText;
        if (jt.conditionGlobal != null && jt.conditionGlobal != undefined && jt.conditionGlobalToggle){
          newElement.conditionGlobal = jt.conditionGlobal;
        }
        if (jt.uid){
          newElement.uid = jt.uid;
        }
        elements.push(newElement);
      });
      page.elements = elements;
      pages.push(page);
    });

    let disableGlobal = "";
    let disableGlobal2 = "";
    let disableGlobalArray = [];

    let first = true;
    let first2 = true;
    pages.map((kt, k) => {
      kt.elements.map((lt, l) => {
        if (lt.conditionGlobal) {
          if (first) {
            first = false;
          } else {
            disableGlobal = disableGlobal + " and ";
          }
          disableGlobal = disableGlobal + "{" + lt.name + "} <> '" + lt.choices[lt.conditionGlobal] + "' and {" + lt.name + "} notempty";
          disableGlobalArray.push({
            "type": "expression",
            "text": "Formulario Bloqueado por la pregunta "+lt.name,
            "expression": "{" + lt.name + "} = '" + lt.choices[lt.conditionGlobal] + "'"
          });
          lt.choices.map((mt, m) => {
            if(m!=l){
              if (first2) {
                first2 = false;
              } else {
                disableGlobal2 = disableGlobal2 + " or ";
              }
              disableGlobal2 = disableGlobal2 + "{" + lt.name + "} = '" + mt+"'";
            }
          });
          disableGlobal2 = disableGlobal2 + " or ";
          disableGlobal2 = disableGlobal2 + "{" + lt.name + "} notempty";
        }
      });
    });

    pages.map((kt, k) => {
      // console.log(kt);
      kt.elements.map((lt, l) => {
        // console.log(lt);
        // if (lt.validators && lt.validators.length>0) {
        //   lt.validators=[
        //     ...lt.validators,
        //     ...disableGlobalArray
        //   ]
        // }else{
        //   lt.validators = disableGlobalArray;
        // }

        // if (lt.validators && lt.validators.length>0) {
        //   lt.validators.push({
        //     "type": "expression",
        //     "text": "Formulario Bloqueado",
        //     "expression": disableGlobal
        //   })
        // }else{
        //   lt.validators=[{
        //     "type": "expression",
        //     "text": "Formulario Bloqueado",
        //     "expression": disableGlobal
        //   }];
        // }
        if (!lt.conditionGlobal) {
          // lt.enableIf = disableGlobal;
        }
      });
    });

    this.survey.pages = pages;
    // let user = JSON.parse(localStorage.getItem("profile")).id;

    if(seemsName){
      swal.fire('Las preguntas en la misma sesión, deben tener nombres diferentes.', '', 'warning');
    }else{
      console.log(this.survey);
      console.log(this.id);
      if (this.id) {
        this.survey.uid = this.id;
        console.log(this.survey);
        console.log(JSON.stringify(this.survey));
        let data={
          "template": {
            "name": this.survey.name,
            "description": this.survey.description,
            "custom": this.survey,
            "type":"C",
            "active":true
          }
        }
        this.formulariosService.saveFormulario(this.id,data).subscribe(
          rt => {
            console.log(rt);

            // this.survey.title = rt.name;
            // this.secciones = rt.data;
          }
        )
      } else {
        let data={
          "name": this.survey.name,
          "description": this.survey.description,
          "custom": this.survey,
          "type":"C",
          "active":true
        }
        console.log(JSON.stringify(data));
        this.formulariosService.createFormulario(data).subscribe(
          rt => {
            console.log(rt);
            this.router.navigate(['/formularios/survey?id='+rt.data.id])
          }
        )
      }
      this.survey.showCompletedPage = false;
      // localStorage.setItem("survey", JSON.stringify(this.survey));

      this.survey["ind"]=this.id-1;
      localStorage.setItem("survey"+this.id,JSON.stringify(this.survey));
      // this.router.navigate(['/formularios/survey']);
      window.open('/formularios/survey?id='+this.id, '_blank');
    }
  }
}

