import { Component, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { PageEvent } from "@angular/material/paginator";
// import { MatSelectChange } from "@angular/material/select";
// import { TableColumn } from "@lib/interfaces/table-column.interface";
import { Request } from '@lib/models/request/request.model';
// import { RequestService } from "app/services/request/request.service";
// import { Customer, aioTableData } from "app/shared/table/custumer.mode";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
// import { UserService } from "app/services/user/user.service";

// import { SecurityService } from "app/services/security/security.service";

import {
  ActionsModel,
  EventTableModel,
  TableColumnsModel,
} from "@lib/shared/table/table.model";

@Component({
  selector: "app-request",
  templateUrl: "./request.component.html",
  styleUrls: ["./request.component.scss"],
})
export class RequestComponent implements OnInit {
  data: any[]=[];
  columns: TableColumnsModel[] = [
    {
      label: "Id",
      property: "idRequest",
      type: "text",
      visible: true,
    },
    {
      label: "Categoría",
      property: "Category",
      type: "text",
      visible: true,
    },
    {
      label: "Identificación",
      property: "custom.identification",
      type: "text",
      visible: true,
    },
    {
      label: "Fecha",
      property: "custom.time",
      type: "text",
      visible: true,
    },
    {
      label: "Estado",
      property: "statusName",
      type: "labels",
      visible: true,
    },
    { label: "Opciones", property: "actions", type: "actions", visible: false },
  ];
  menuActions: ActionsModel[] = [];
  pageSize = 5;
  page = 1;
  totalItems=0;
  searchText: string = "";
  status=0;
  typeRequests=0;
  selectStatus = new FormControl<number>(0);
  selectTypeRequest = new FormControl<number>(0);
  date = new FormControl<string>("");
  currentUrl=0;
  btns=0;
  constructor(
    // private requestService: RequestService,
    private _dialog: MatDialog,
    // private _user: UserService,
    private router: Router,
    // private _securityService: SecurityService
  ) {}

  async ngOnInit() {
    // this.getData();
    // this.getStatus();
    // this.getTypeRequest();
    // this.currentUrl = this.router.url;
    // this._securityService
    //   .getOptions({ route: this.currentUrl })
    //   .subscribe((res) => {
    //     this.btns = res.data;
    //     this.constructorBtnAccion();
    //   });

    // const dialogRef = this._dialog.open(RestoreComponent, {
    //   width: '95%',
    //   height: 'auto',
    //   maxWidth: '1020px',
    //   data: {
    //     user: {
    //       user:"",
    //       identification:"",
    //       cellPhone:""
    //     },
    //     data:{
    //       Category: "d"
    //     }
    //   }
    // });
  }

  async validateAcess(id: string) {
    // let aut = await this._securityService.validateAcess2(id, this.btns);
    // return aut;
  }

  async constructorBtnAccion() {
    // const opciones: TableColumnsModel = this.columns[this.columns.length - 1];
    // if (await this.validateAcess("btn_see")) {
    //   this.menuActions.push({
    //     text: "Ver Solicitud",
    //     icon: "visibility",
    //     property: "ver",
    //   });

    //   this.columns[this.columns.length - 1] = {
    //     ...opciones,
    //     visible: true,
    //   };
    // }
    // if (await this.validateAcess("btn_attend")) {
    //   this.menuActions.push({
    //     text: "Atender Solicitud",
    //     icon: "assignment_late",
    //     property: "atender",
    //   });
    //   this.columns[this.columns.length - 1] = {
    //     ...opciones,
    //     visible: true,
    //   };
    // }
  }

  createRequest() {
    // const dialogRef = this._dialog.open(NewRequestComponent, {
    //   width: "80%",
    //   height: "auto",
    //   maxWidth: "80%",
    //   panelClass: "my-dialog-container-class",
    // });
    // dialogRef.afterClosed().subscribe((data) => {
    //   this.getData();
    //   if (data) {
    //   }
    // });
  }
  search(text:any) {
    // this.searchText = text;
    // this.getData();
  }
  getData(page:any?) {
    // this.requestService
    //   .getRequest(
    //     {
    //       page: page || this.page,
    //       pageSize: this.pageSize,
    //       searchText: this.searchText,
    //     },
    //     {
    //       idRequestType: this.selectTypeRequest.value || null,
    //       idStatus: this.selectStatus.value || null,
    //       date: this.date.value || null,
    //     }
    //   )
    //   .subscribe((data: any) => {
    //     console.log(data);
    //     this.data = data.data.map((request) => new Request(request));
    //     this.totalItems = data.pagination.totalItems;
    //   });
  }
  getStatus() {
    // this.requestService.getStatus(2).subscribe({
    //   next: (data: any) => {
    //     this.status = data.data;
    //   },
    // });
  }
  getTypeRequest() {
    // this.requestService.getTypeRequest().subscribe({
    //   next: (data: any) => {
    //     this.typeRequests = data.data;
    //   },
    // });
  }
  nextPage(page: PageEvent) {
    this.pageSize = page.pageSize;
    this.getData(page.pageIndex);
  }
  opciones(datos: EventTableModel<Request, "ver" | "atender">) {
  //   if (datos.event === "atender") {
  //     //Acción a realizar
  //     console.log(datos.data, datos.event);
  //     this._user.getInfoUserRecovery(datos.data.idRequest).subscribe((res) => {
  //       console.log(res);
  //       let data = {
  //         id: datos.data.idRequest,
  //         user: res.data,
  //         data: datos.data["request"],
  //       };
  //       const dialogRef = this._dialog.open(RestoreComponent, {
  //         width: "95%",
  //         height: "auto",
  //         maxWidth: "1020px",
  //         data: data,
  //       });
  //       dialogRef.afterClosed().subscribe((data) => {
  //         console.log(datos);
  //         console.log(data);
  //         if (data) {
  //           if (datos.data["request"]["idCategory"] == 1) {
  //             if (data.data.password) {
  //               this.getData();
  //               const dialogRef = this._dialog.open(NewPasswordComponent, {
  //                 width: "95%",
  //                 height: "auto",
  //                 maxWidth: "400px",
  //                 data: data.data,
  //               });
  //               dialogRef.afterClosed().subscribe((data) => {});
  //             } else {
  //               swal.fire(
  //                 "Se ha rechazado la solicitud exitosamente.",
  //                 "",
  //                 "info"
  //               );
  //             }
  //           }else{
  //             swal.fire('Se ha rechazado la solicitud exitosamente.', '', 'success')
  //           }
  //           if (datos.data["request"]["idCategory"] == 2) {
  //             this.getData();
  //             if (data.aprobado) {
  //               swal.fire(
  //                 "Se ha aprobado la solicitud exitosamente.",
  //                 "",
  //                 "info"
  //               );
  //             } else {
  //               swal.fire(
  //                 "Se ha rechazado la solicitud exitosamente.",
  //                 "",
  //                 "info"
  //               );
  //             }
  //           }
  //         }
  //       });
  //     });
  //     return;
  //   }
  //   if (datos.event === "ver") {
  //     //Acción a realizar
  //     console.log(datos.data, datos.event);
  //   }
  // }
}
