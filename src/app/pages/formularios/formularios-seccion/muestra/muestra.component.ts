import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import swal from 'sweetalert2';

@Component({
  selector: 'vex-muestra',
  templateUrl: './muestra.component.html',
  styleUrls: ['./muestra.component.scss']
})
export class MuestraComponent implements OnInit {

  @Input() question: any = null;
  @Input() indexQuestion: any = null;
  @Output() changeCheckDate: any = new EventEmitter<any>();

  title="";
  html="";

  constructor() { }

  ngOnInit(): void {
    // console.log(this.question);
  }

  handleUpload(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      console.log(reader.result);
      this.question.image=reader.result;
      // let data: any = {
      //   // "uid": "bW7PpHz31RaKfNgk83lB",
      //   "name": this.name,
      //   "description": this.description,
      //   "user": JSON.parse(localStorage.getItem("profile")).id,
      //   "data": reader.result
      //   // "columns": this.columns,
      //   // "data": table
      // }
    };
  }

  changeAnswerHandle(e){
    // this.changeAnswer.emit(e.target.value);
  }

}
