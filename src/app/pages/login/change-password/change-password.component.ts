import { CommonModule } from '@angular/common';
import { Component, Input, OnInit, inject } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { storage } from '@lib/utils/storage/storage.utils';

import { Router } from '@angular/router';
import { AuthService } from '@lib/services';

import { RouterModule } from '@angular/router';
import Swal from 'sweetalert2';
import { environment } from '../../../../environments/environment';

@Component({
    standalone: true,
    imports: [CommonModule, RouterModule, FormsModule, MatInputModule, MatFormFieldModule, ReactiveFormsModule],
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {
    changePasswordForm!: FormGroup;
    typePassword: any = 'password';
    typePassword2: any = 'password';

    @Input() returnUrl!: string;
    private readonly _router = inject(Router);
    private readonly _authService = inject(AuthService);
    constructor(
        // private _changePasswordService: ChangePasswordService,
        // private router: Router,
        // private _dialog: MatDialog,
        private fb: FormBuilder,
    ) {}

    ngOnInit(): void {
        this.changePasswordForm = this.fb.group({
            currentPassword: new FormControl('', [Validators.required]),
            password: new FormControl(
                '',
                Validators.compose([
                    Validators.required,
                    (c: AbstractControl) => Validators.required(c),
                    Validators.pattern(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*#?&^_\-+\/]).{8,}/),
                ]),
            ),
            password2: new FormControl(
                '',
                Validators.compose([
                    Validators.required,
                    (c: AbstractControl) => Validators.required(c),
                    Validators.pattern(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*#?&^_\-+\/]).{8,}/),
                ]),
            ),
        });
    }

    seePassword() {
        this.typePassword = this.typePassword == 'password' ? 'text' : 'password';
    }
    seePassword2() {
        this.typePassword2 = this.typePassword2 == 'password' ? 'text' : 'password';
    }

    changePassword() {
        for (var i in this.changePasswordForm.controls) {
            this.changePasswordForm.controls[i].markAsTouched();
            this.changePasswordForm.controls[i].markAsDirty();
        }
        if (this.changePasswordForm.valid) {
            if (this.changePasswordForm.controls['password'].value == this.changePasswordForm.controls['passwor2'].value) {
                let user: any = storage.getItem('appSession');
                console.log(user);
                let data = {
                    currentPassword: this.changePasswordForm.controls['currentPassword'].value,
                    userName: user.userName,
                    newPassword: this.changePasswordForm.controls['password2'].value,
                    token: user.token,
                };
                if (environment.production) {
                    this._authService.changePassword(data).subscribe((data: any) => {
                        if (data.isSuccess) {
                            this._authService.autoLogin();
                            this._router.navigate(['/']);
                        } else {
                            Swal.fire('Cambio de contraseña', data.message, 'warning');
                        }
                    });
                } else {
                    this._router.navigate(['/']);
                }
            } else {
                Swal.fire('Login', 'Las contraseñas no coinciden.', 'warning');
            }
        }
    }

    login() {
        this._router.navigate(['/']);
    }
}


