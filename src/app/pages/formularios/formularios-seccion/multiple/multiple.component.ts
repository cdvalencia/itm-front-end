import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'vex-multiple',
  templateUrl: './multiple.component.html',
  styleUrls: ['./multiple.component.scss']
})
export class MultipleComponent implements OnInit {


  @Input() options: any;
  @Input() survey: any;
  @Input() other: any;
  @Output() changeOther: any = new EventEmitter<any>();
  @Output() changeSelect: any = new EventEmitter<any>();

  options2=[]

  constructor() { }

  ngOnInit(): void {
    this.options2 = this.options;
  }

  addOption(ind: any){
    // console.log(data);
    this.options.push({
      text: "",
      valor: 1,
      check: false
    });
  }

  changeSelectHanlde(ind: any){
    this.changeSelect.emit(ind);
  }

  deleteOption(ind: any){
    // this.options.pop();
    if (ind > -1) {
      this.options.splice(ind, 1);
    }
  }
}
