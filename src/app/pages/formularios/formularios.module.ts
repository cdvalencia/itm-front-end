import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser'
import { FormulariosRoutingModule } from './formularios-routing.module';

import { FormulariosComponent } from './formularios.component';
import { NewFormComponent } from './new-form/new-form.component';
import { FormulariosSeccionComponent } from './formularios-seccion/formularios-seccion.component';

import { TableModule } from "app/shared/table/table.module";
import { PageLayoutModule } from "@vex/components/page-layout/page-layout.module";
import { MultipleComponent } from './formularios-seccion/multiple/multiple.component';
import { MultipleValuesComponent } from './formularios-seccion/multiple-values/multiple-values.component';
import { FechaComponent } from './formularios-seccion/fecha/fecha.component';
import { MuestraComponent } from './formularios-seccion/muestra/muestra.component';
import { SimpleComponent } from './formularios-seccion/simple/simple.component';
import { ArchivoComponent } from './formularios-seccion/archivo/archivo.component';
import { ListaComponent } from './formularios-seccion/lista/lista.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { DirectivesModule } from '../../../directives/directives.module';
import { FormulariosService } from '../../../app/services/formularios/formularios.service';
import { MatFormFieldModule } from "@angular/material/form-field";
import { HeaderModule } from "app/shared/layouts/header/header.module";
import { InputSearchModule } from "app/shared/inputs/search/search.module";
import { ButtonFilterModule } from "app/shared/button/button-filter/button-filter.module";

@NgModule({
  declarations: [
    FormulariosComponent,
    NewFormComponent,
    FormulariosSeccionComponent,
    MultipleComponent,
    MultipleValuesComponent,
    FechaComponent,
    MuestraComponent,
    SimpleComponent,
    ArchivoComponent,
    ListaComponent,
  ],
  imports: [
    CommonModule,
    TableModule,
    HeaderModule,
    PageLayoutModule,
    InputSearchModule,
    ButtonFilterModule,
    FormulariosRoutingModule,
    CommonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    DirectivesModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class FormulariosModule { }
