import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'vex-simple',
  templateUrl: './simple.component.html',
  styleUrls: ['./simple.component.scss']
})
export class SimpleComponent implements OnInit {

  @Input() typeQuestion: any= null;
  @Input() correctAnswer: any= null;
  @Input() sizeMax: any= null;
  @Input() survey: any;  
  @Output() changeAnswer: any = new EventEmitter<any>(); 
  @Output() changeSize: any = new EventEmitter<any>(); 
  

  constructor() { }

  ngOnInit(): void {
  }

  changeAnswerHandle(e){
    this.changeAnswer.emit(e.target.value);
  }

  changeSizeHandle(e){
    console.log(e);    
    this.changeSize.emit(e.target.value);
  }

}
