import { CommonModule } from '@angular/common';
import { Component, Input, OnInit, inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { Router } from '@angular/router';
import { AuthService } from '@lib/services';

import { RouterModule } from '@angular/router';

@Component({
    standalone: true,
    imports: [CommonModule, RouterModule, FormsModule, MatInputModule, MatFormFieldModule, ReactiveFormsModule ],        
    templateUrl: './recovery.component.html',
    styleUrls: ['./recovery.component.scss'],
})
export class RecoveryComponent implements OnInit {
    recoveryForm!: FormGroup;    
    typePassword: any = 'password';    

    @Input() returnUrl!: string;
    private readonly _router = inject(Router);
    private readonly _authService = inject(AuthService);
    constructor(
        // private _recoveryService: RecoveryService,
        // private router: Router,
        // private _dialog: MatDialog,
        private fb: FormBuilder,
    ) {}

    ngOnInit(): void {
        this.recoveryForm = this.fb.group({
            identification: new FormControl('', [Validators.required]),
            password: new FormControl('', [Validators.required]),
        });
    }    

    recovery() {        
        for (var i in this.recoveryForm.controls) {
            this.recoveryForm.controls[i].markAsTouched();
            this.recoveryForm.controls[i].markAsDirty();
        }
        if (this.recoveryForm.valid) {
        }
    }

    login() {                
        this._router.navigate(['/login'])
    }    
}


