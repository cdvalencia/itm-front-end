import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulariosTablaComponent } from './formularios-tabla.component';

describe('FormulariosTablaComponent', () => {
  let component: FormulariosTablaComponent;
  let fixture: ComponentFixture<FormulariosTablaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormulariosTablaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormulariosTablaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
