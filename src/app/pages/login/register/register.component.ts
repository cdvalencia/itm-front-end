import { CommonModule } from '@angular/common';
import { Component, Input, OnInit, inject } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';

import { Router } from '@angular/router';
import { AuthService } from '@lib/services';

import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { RouterModule } from '@angular/router';
import { NumbersOnlyDirective } from '@lib/directives/numbers-only/numbers-only.directive';
import Swal from 'sweetalert2';

import { environment } from '../../../../environments/environment';

@Component({
    standalone: true,
    imports: [
        NumbersOnlyDirective,
        CommonModule,
        RouterModule,
        FormsModule,
        MatInputModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatSelectModule,
        MatFormFieldModule,        
        MatDatepickerModule,
        MatNativeDateModule,
    ],
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
    registerForm!: FormGroup;
    typePassword: any = 'password';
    typePassword2: any = 'password';
    documents: any = [];
    cities: any= [];
    @Input() returnUrl!: string;
    private readonly _router = inject(Router);
    private readonly _authService = inject(AuthService);
    constructor(private fb: FormBuilder) {}

    ngOnInit(): void {
        this.registerForm = this.fb.group({
            type: new FormControl('', [Validators.required]),
            documento: new FormControl('', [Validators.required]),
            name: new FormControl('', [Validators.required]),
            lastName: new FormControl('', [Validators.required]),
            email: new FormControl(
                '',
                Validators.compose([
                    Validators.required,
                    Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'),
                ]),
            ),
            dateExpedition: new FormControl('', [Validators.required]),
            lugar: new FormControl('', [Validators.required]),
            date: new FormControl('', [Validators.required]),
            ciudad: new FormControl('', [Validators.required]),
            password: new FormControl(
                '',
                Validators.compose([
                    Validators.required,
                    (c: AbstractControl) => Validators.required(c),
                    Validators.pattern(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*#?&^_\-+\/]).{8,}/),
                ]),
            ),
            password2: new FormControl(
                '',
                Validators.compose([
                    Validators.required,
                    (c: AbstractControl) => Validators.required(c),
                    Validators.pattern(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*#?&^_\-+\/]).{8,}/),
                ]),
            ),
        });

        if (environment.production){
            this.cities = [
                {
                    id: 4,
                    name: 'Medellín',
                },
            ];
            this.documents = [
                {
                    IdTipoDocumento: 1,
                    TipoDocumento1: 'Cedula de ciudadania',
                },
                {
                    IdTipoDocumento: 2,
                    TipoDocumento1: 'Cedula Extranjera',
                },
            ];
        }else{
            this.cities = [
                {
                    id: 4,
                    name: 'Medellín',
                },                
            ];
            this.documents = [
                {
                    IdTipoDocumento: 1,
                    TipoDocumento1: 'Cedula de ciudadania',
                },
                {
                    IdTipoDocumento: 2,
                    TipoDocumento1: 'Cedula Extranjera',
                },
            ];
        }        
    }

    register() {
        for (var i in this.registerForm.controls) {
            this.registerForm.controls[i].markAsTouched();
            this.registerForm.controls[i].markAsDirty();
        }
        console.log(this.registerForm);
        console.log(this.registerForm.value);
        console.log(this.registerForm.valid);
        // Object.keys(this.registerForm.controls).forEach((key: any) => {
        //     this.registerForm.get(key).setErrors(null);
        // });
        if (this.registerForm.valid) {
            if (this.registerForm.controls['password'].value == this.registerForm.controls['passwor2'].value){
                let data = {
                    CorreoElectronico: this.registerForm.controls['email'].value,
                    Apellidos: this.registerForm.controls['lastName'].value,
                    Nombres: this.registerForm.controls['name'].value,
                    Documento: this.registerForm.controls['documento'].value,
                    IdTipoDocumento: this.registerForm.controls['type'].value,
                    IdCiudadDocumento: this.registerForm.controls['lugar'].value,
                    FechaExpedicion: this.registerForm.controls['dateExpedition'].value,
                    Clave: this.registerForm.controls['password'].value,
                };
                console.log(data);            
                this._authService.register(data).subscribe((data: any) => {                    
                    if (data.isSuccess) {
                        Swal.fire('Register', "Usuario registrado, exitosamente.", 'success');
                        this.registerForm.reset();                        
                    } else {
                        Swal.fire('Login', data.message, 'warning');
                    }
                });
            }else{
                Swal.fire('Login', "Las contraseñas no coinciden.", 'warning');
            }
        }
    }

    changePassword() {
        console.log();
    }
    changePassword2() {
        console.log();
    }
    seePassword() {
        this.typePassword = this.typePassword == 'password' ? 'text' : 'password';
    }
    seePassword2() {
        this.typePassword2 = this.typePassword2 == 'password' ? 'text' : 'password';
    }
    login() {
        this._router.navigate([this.returnUrl ?? `/login`]);
    }
}


