import { Routes } from '@angular/router';

export const routes: Routes = [
    {
        path: '',
        title: 'Files',
        loadComponent: async () => (await import('./files.component')).FilesComponent,
    },
];
