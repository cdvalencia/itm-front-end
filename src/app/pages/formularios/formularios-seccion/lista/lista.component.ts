import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormulariosService } from '../../../../../app/services/formularios/formularios.service';

@Component({
  selector: 'vex-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.scss']
})
export class ListaComponent implements OnInit {

  @Input() question: any = null;
  lista = {};
  listas = [];
  muestras = [];
  @Output() changeSelect: any = new EventEmitter<any>();



  constructor(
    private formulariosService: FormulariosService
  ) { }

  compareFn(role1: any, role2: any) {
    return role1 && role2 ? role1.id === role2.id : role1 === role2;
  }

  ngOnInit(): void {
    // let token = JSON.parse(localStorage.getItem("user")).stsTokenManager.accessToken;
    // this.formulariosService.getListas(token).subscribe(
    //   rt => {
    //     this.listas = rt.data;
    //   }
    // )
  }

  changeType(e, indexQuestion) {
    console.log(e, indexQuestion);
    let token = JSON.parse(localStorage.getItem("user")).stsTokenManager.accessToken;
    this.formulariosService.getLista(e.uid, token).subscribe(
      rt => {
        console.log(rt);
        this.muestras = rt.data;
      }
    )
    this.changeSelect.emit(e, indexQuestion);
  }
}
