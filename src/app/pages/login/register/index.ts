import { Routes } from '@angular/router';

export const routes: Routes = [
    {
        path: '',
        title: 'Register',
        loadComponent: async () => (await import('./register.component')).RegisterComponent,
    },
];
