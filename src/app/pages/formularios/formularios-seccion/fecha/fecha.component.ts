import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'vex-fecha',
  templateUrl: './fecha.component.html',
  styleUrls: ['./fecha.component.scss']
})
export class FechaComponent implements OnInit {

  @Input() question: any = null;
  @Input() indexQuestion: any = null;
  @Output() changeCheckDate: any = new EventEmitter<any>();  

  constructor() { }

  ngOnInit(): void {
    // console.log(this.question);
  }

  changeCheckDateHandle(e) {
    console.log(e.target.value);
    let data = {      
      value: e.target.value,
      question: this.indexQuestion
    }
    this.changeCheckDate.emit(data);
  }

}
