import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { Model, StylesManager, SurveyNG } from "survey-angular";
import swal from 'sweetalert2';

import { themeJson } from './theme';

// import { FormulariosService } from '../../../../app/services/formularios/formularios.service';

// import "survey-angular/defaultV2.min.css";
// "node_modules/survey-angular/defaultV2.min.css"
// defaultV2
// "node_modules/survey-angular/modern.min.css",
// modern
// StylesManager.applyTheme("modern");

import { survey as surveyJson } from './survey';


StylesManager.applyTheme("defaultV2");

@Component({
  standalone: true,
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss'],
})
export class SurveyComponent implements OnInit {

  survey: any={};
  surveyJson: any={};
  canComplete = true;
  messageComplete = "";
  id=0;
  constructor(
    // private formulariosService: FormulariosService,
    private route: ActivatedRoute
  ) {
  }

  alertResults(survey: any, sender: any) {
    console.log(this.canComplete);
    if (this.canComplete) {
      const results = JSON.stringify(sender.data);
      console.log({survey});
      // navigator.clipboard.writeText(results).then(function () {
      //   console.log('Async: Copying to clipboard was successful!');
      // }, function (err) {
      //   console.error('Async: Could not copy text: ', err);
      // });
      swal.fire(
        {
          title: "Cuestionario",
          text: "Formulario complet.",
          icon: 'warning',
          showCancelButton: false,
          confirmButtonText: 'Cerrar',
        }
      )
      console.log(results);
    }else{
      swal.fire(
        {
          title: "Cuestionario",
          text: this.messageComplete,
          icon: 'warning',
          showCancelButton: false,
          confirmButtonText: 'Cerrar',
        }
      )
      survey.clear(false, true);
      survey.render();
    }
  }
  ngOnInit() {
    console.log(this.route.queryParams);
    this.initSurvey();
    // if((this.route.queryParams["_value"].id) || false){
    //   this.formulariosService.getFormulario(this.route.queryParams["_value"].id).subscribe(
    //     rt => {
    //       console.log(rt);
    //       this.surveyJson = rt.data.custom;
    //       this.surveyJson.name = rt.data.name;
    //       this.surveyJson.description = rt.data.description;
    //       this.surveyJson.showCompletedPage = false;
    //       this.initSurvey();
    //     }
    //   )
    // }else{
    //   this.id=(this.route.queryParams["_value"].id)?this.route.queryParams["_value"].id:1;
    //   this.surveyJson=(localStorage.getItem("survey"+this.id))?JSON.parse(localStorage.getItem("survey"+this.id)):{}
    //   console.log(this.surveyJson);
    //   let promisesListas=[];
    //   this.surveyJson.showCompletedPage = false;
    //   this.surveyJson.pages.map((it,i)=>{
    //     it.elements.map((jt,j)=>{
    //       if(jt.lista){
    //         const promise = new Promise((resolve, reject) => {
    //           let token = JSON.parse(localStorage.getItem("user")).stsTokenManager.accessToken;
    //           this.formulariosService.getLista(jt.lista.uid,token).subscribe(
    //             rt => {
    //               console.log(rt);
    //               resolve([i,j,(rt && rt.data)?rt.data:[]]);
    //             }
    //           )
    //         });
    //         promisesListas.push(promise);
    //       }
    //     })
    //   })

    //   Promise.all(promisesListas).then((values) => {
    //     console.log(values);
    //     values.map((it, j) => {
    //       this.surveyJson.pages[it[0]].elements[it[1]].choices = it[2];
    //     })
    //     this.initSurvey();
    //   });
    // }
  }

  completing(sender: any, options: any) {
    console.log("completing", sender, options);
    this.canComplete = true;

    // this.surveyJson.pages.map((it, i) => {
    //   it.elements.map((jt, j) => {
    //     if (sender.data[jt.title]) {
    //       if (Array.isArray(jt.conditionGlobal)){
    //         jt.conditionGlobal.map((kt, k) => {
    //           if (jt.choices && jt.choices[kt] == sender.data[jt.title]) {
    //             // console.log((jt.choices[jt.conditionGlobal], sender.data[jt.title]));
    //             this.canComplete = false;
    //             this.messageComplete = (jt.conditionGlobalText) ? jt.conditionGlobalText:"No se puede Completar este formulario, pues la pregunta '" + jt.title + "' no puede ser '" + sender.data[jt.title] + "'.";
    //           }
    //         })
    //       }else{
    //         if (jt.choices && jt.choices[jt.conditionGlobal] == sender.data[jt.title]) {
    //           // console.log((jt.choices[jt.conditionGlobal], sender.data[jt.title]));
    //           this.canComplete = false;
    //           this.messageComplete = (jt.conditionGlobalText) ? jt.conditionGlobalText : "No se puede Completar este formulario, pues la pregunta '" + jt.title + "' no puede ser '" + sender.data[jt.title] + "'.";
    //         }
    //       }
    //     }
    //   })
    // });
  }
  completed(sender: any, options: any) {
    console.log("completed", sender, options);
  }

  initSurvey(){
    this.survey = new Model(surveyJson);    
    this.survey.showLogicTab = false;
    this.survey.focusFirstQuestionAutomatic = false;
    this.survey.applyTheme(themeJson);
    this.survey.onComplete.add((survey: any, sender: any) => {
      console.log(this.canComplete);
      if (this.canComplete) {
        const results = JSON.stringify(sender.data);
        console.log(survey);
        // navigator.clipboard.writeText(results).then(function () {
        //   console.log('Async: Copying to clipboard was successful!');
        // }, function (err) {
        //   console.error('Async: Could not copy text: ', err);
        // });
        swal.fire(
          {
            title: "Cuestionario",
            text: "Formulario completado.",
            icon: 'warning',
            showCancelButton: false,
            confirmButtonText: 'Cerrar',
          }
        )
        console.log(results);
      } else {
        swal.fire(
          {
            title: "Cuestionario",
            text: this.messageComplete,
            icon: 'warning',
            showCancelButton: false,
            confirmButtonText: 'Cerrar',
          }
        )
        survey.clear(false, true);
        survey.render();
      }
    });
    SurveyNG.render("surveyElement", {
      model: this.survey,
      showCompletedPage: true,
      onCompleting: (sender: any, options: any) => this.completing(sender, options),
      onCompleted: (sender: any, options: any) => this.completed(sender, options),
    });
  }
}
