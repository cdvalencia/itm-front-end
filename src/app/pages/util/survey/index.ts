import { Routes } from '@angular/router';

export const routes: Routes = [
    {
        path: '',
        title: 'Survey',
        loadComponent: async () => (await import('./survey.component')).SurveyComponent,
    },
];
