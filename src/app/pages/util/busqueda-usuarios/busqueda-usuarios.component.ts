import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, ChangeDetectionStrategy, Component, OnInit, inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { RouterModule } from '@angular/router';
import { ThemeService } from '@lib/services/theme';
import { storage } from '@lib/utils/storage/storage.utils';

import { Subject } from 'rxjs';

export interface itemElement {
    id: number;
    name: string;
    type: number;
}
@Component({
    standalone: true,
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    imports: [CommonModule, RouterModule, FormsModule, MatFormFieldModule, ReactiveFormsModule, MatInputModule],
    templateUrl: './busqueda-usuarios.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BusquedaUsuariosComponent implements OnInit {
    private readonly _themeService = inject(ThemeService);

    private readonly _destroy$ = new Subject();
    searchForm: FormGroup;
    resultForm: FormGroup;
    resultVisible = false;
    ngOnInit(): void {}

    constructor(private fb: FormBuilder) {
        this.searchForm = this.fb.group({
            search: new FormControl('', [Validators.required]),
        });
        this.resultForm = this.fb.group({
            name: new FormControl('', [Validators.required]),
            lastName: new FormControl('', [Validators.required]),
            userName: new FormControl('', [Validators.required]),
            eMail: new FormControl('', [Validators.required]),
        });
    }

    search() {
        this.resultVisible = true;
        let user: any = storage.getItem('appSession');
        this.resultForm.controls['name'].setValue(user.name);
        this.resultForm.controls['lastName'].setValue(user.lastName);
        this.resultForm.controls['userName'].setValue(user.userName);
        this.resultForm.controls['eMail'].setValue(user.eMail);
    }
}
