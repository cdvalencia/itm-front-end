import { Routes } from '@angular/router';

export const routes: Routes = [
    {
        path: '',
        title: 'Recovery',
        loadComponent: async () => (await import('./recovery.component')).RecoveryComponent,
    },
];
