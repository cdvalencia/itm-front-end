import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Output, inject } from '@angular/core';
import { NavigationStart, Router, RouterModule } from '@angular/router';
import { AuthService } from '@lib/services';
import { LogoComponent } from '../logo/logo.component';

@Component({
    selector: 'app-navbar-content',
    standalone: true,
    imports: [CommonModule, RouterModule, LogoComponent],
    templateUrl: './navbar-content.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavbarContentComponent {
    private readonly _router = inject(Router);
    private readonly _authService = inject(AuthService);
    @Output() changeMenu = new EventEmitter<boolean>();

    menu: any = [
        {
            id: 1,
            text: 'Administrador 1',
            ico: 'icon-[fe--users]',
            children: [
                {
                    id: 2,
                    text: 'Home',
                    route: '/',
                    ico: 'icon-[fe--users]',
                },
                {
                    id: 2,
                    text: 'Archivos',
                    route: '/files',
                    ico: 'icon-[foundation--torso-business]',
                },
                {
                    id: 3,
                    text: 'Cuestionario',
                    route: '/cuestionario',
                    ico: 'icon-[foundation--torso-business]',
                },
                {
                    id: 3,
                    text: 'Búsqueda de usuarios',
                    route: '/busqueda-usuarios',
                    ico: 'icon-[foundation--torso-business]',
                },
                {
                    id: 2,
                    text: '3er Level',
                    ico: 'icon-[foundation--torso-business]',
                    children: [
                        {
                            id: 2,
                            text: 'Menu1',
                            route: '/home2',
                            ico: 'icon-[fe--users]',
                        },
                        {
                            id: 2,
                            text: 'Menu 2',
                            route: '/files2',
                            ico: 'icon-[foundation--torso-business]',
                        },
                        {
                            id: 2,
                            text: 'Menu 3',
                            route: '/files2',
                            ico: 'icon-[foundation--torso-business]',
                        },
                    ],
                },
            ],
        },
        {
            id: 2,
            text: 'Administrador 2',
            ico: 'icon-[foundation--torso-business]',
            children: [
                {
                    id: 2,
                    text: 'Usuarios',
                    route: '/settings/account',
                    ico: 'icon-[fe--users]',
                },
                {
                    id: 2,
                    text: 'Roles',
                    route: '/settings/accessibility',
                    ico: 'icon-[foundation--torso-business]',
                },
            ],
        },
    ];

    currentPage: String = '';
    constructor(private router: Router) {
        this._router.events.subscribe((events) => {
            if (events instanceof NavigationStart) {
                this.currentPage = events.url;
            } else {
                this.currentPage = this.router.url;
            }            
        });
    }

    openMenu(ind: any) {
        let opened = this.menu[ind].open;
        this.menu.map((it: any) => {
            it.open = false;
        });
        this.menu[ind].open = !opened;
    }

    openMenu2(i: any, j: any) {
        let opened = this.menu[i].children[j].open;
        this.menu.map((it: any) => {
            it.children.map((jt: any) => {
                jt.open = false;
            });
        });
        this.menu[i].children[j].open = !opened;
    }
}
