import { CommonModule } from '@angular/common';
import { Component, Input, OnInit, inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { storage } from '@lib/utils/storage/storage.utils';

import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '@lib/services';

import { RouterModule } from '@angular/router';
import Swal from 'sweetalert2';
import { environment } from '../../../environments/environment';

@Component({
    standalone: true,
    imports: [CommonModule, RouterModule, FormsModule, MatInputModule, MatFormFieldModule, ReactiveFormsModule],
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
    loginForm!: FormGroup;
    password: any = '';
    step: any = 1;
    typePassword: any = 'password';
    typeRecovery: any = 'A';
    title: any = 'app';
    elementType: any = 'url';
    value: any = 'Techiediaries';

    @Input() returnUrl!: string;
    private readonly _router = inject(Router);
    private readonly _authService = inject(AuthService);
    constructor(
        // private _loginService: LoginService,
        // private router: Router,
        // private _dialog: MatDialog,
        private fb: FormBuilder,
        private activatedRoute: ActivatedRoute,
    ) {}

    ngOnInit(): void {
        this.loginForm = this.fb.group({
            userName: new FormControl('', [Validators.required]),
            password: new FormControl('', [Validators.required]),
        });
    }

    loginService() {
        //   const password = this.loginForm.get("password").getRawValue();
        //   const hashedPassword = sha256.update(password).hex();
        //   let data = {
        //   identification: this.loginForm.get("identification").getRawValue(),
        //   password: hashedPassword,
        // }
        // return this._loginService.login(data);
    }

    login() {
        for (var i in this.loginForm.controls) {
            console.log(i);
            this.loginForm.controls[i].markAsTouched();
            this.loginForm.controls[i].markAsDirty();
        }
        if (this.loginForm.valid) {
            let data = {
                userName: this.loginForm.controls['userName'].value,
                password: this.loginForm.controls['password'].value,
            };
            console.log(environment.production);
            if (environment.production) {
                this._authService.login(data).subscribe((data: any) => {
                    console.log(data);
                    console.log(data.idUser);
                    console.log(!data.idUser);
                    if (data.isSuccess) {
                        storage.setItem('appSession', data.data);
                        if (data.data.mustChangePass) {
                            this._router.navigate(['/change-password'], { relativeTo: this.activatedRoute });
                        } else {
                            this._authService.autoLogin();
                            this._router.navigate(['/'], { relativeTo: this.activatedRoute });
                        }
                    } else {
                        Swal.fire('Login', data.message, 'warning');
                    }
                });
            } else {
                let user: any = {
                    dateLastIn: '2023-09-20T22:17:37.6803286-05:00',
                    eMail: 'reyblop@hotmail.com',
                    expiresIn: 20399,
                    idUser: 1,
                    lastName: 'Barco Lopez',
                    message: '',
                    mustChangePass: false,
                    name: 'Reinaldo',
                    token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.,eyJuYW1lIjoiOTg1MjQwMDUiLCJuYmYiOjE2OTUyNjYyNTcsImV4cCI6MTY5NTI2ODY1NywiaWF0IjoxNjk1MjY2MjU3fQ.f4_DeVSik8LGn23jTQaOzK8l1TolyIUX5vQM-_MVKF8',
                    userName: '98524005',
                    validated: true,
                };
                storage.setItem('appSession', user);
                this._authService.autoLogin();
                this._router.navigate(['/']);
            }
        }
    }

    changeView(state: any) {}

    recovey() {
        this._router.navigate(['/recovery']);
    }

    changePassword() {
        console.log();
    }

    seePassword() {
        this.typePassword = this.typePassword == 'password' ? 'text' : 'password';
    }

    register() {
        this._router.navigate(['/register']);
    }
}


