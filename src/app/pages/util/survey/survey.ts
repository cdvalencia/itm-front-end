export const survey = {
    title: 'Formulario de Inscripción',
    description: 'Digita la información solicitada.',
    logo: '@/../assets/images/logo.png',
    logoFit: 'cover',
    logoPosition: 'right',
    questionErrorLocation: 'bottom',
    pages: [
        {
            name: 'DATOS PERSONALES',
            title: 'DATOS PERSONALES',
            elements: [
                {
                    type: 'dropdown',
                    name: 'tipo_documento',
                    title: 'Tipo de Documento',
                    // isRequired : true,
                    choices: ['Cedula de Ciudadanía', 'Tarjeta de identidad'],
                },
                {
                    type: 'text',
                    title: 'Documento de Identificación',
                    name: 'Documento de Identificación',
                    startWithNewLine: false,
                    // isRequired: true,
                    maxLength: 25,
                    minLength: 6,
                },
                {
                    type: 'dropdown',
                    name: 'Ciudad de Expedición',
                    title: 'Ciudad de Expedición',
                    startWithNewLine: false,
                    choices: ['Medellín - Antioquía', 'Abejorral - Antioquía'],
                    // isRequired: true,
                },
                {
                    type: 'text',
                    name: 'fecha de expedicion de documento',
                    title: 'fecha de expedicion de documento',
                    // isRequired: true,
                    inputType: 'date',
                },
                {
                    type: 'text',
                    title: 'Nombres',
                    name: 'Nombres',
                    startWithNewLine: false,
                    // isRequired: true,
                    maxLength: 25,
                    minLength: 6,
                },
                {
                    type: 'text',
                    title: 'Apellidos',
                    name: 'Apellidos',
                    startWithNewLine: false,
                    // isRequired: true,
                    maxLength: 25,
                    minLength: 6,
                },

                {
                    type: 'dropdown',
                    name: 'Género',
                    title: 'Género',
                    choices: ['Femenino', 'Masculino'],
                    // isRequired: true,
                },
                {
                    type: 'dropdown',
                    name: 'Estado cívil',
                    title: 'Estado cívil',
                    startWithNewLine: false,
                    choices: ['Soltero', 'Casado', 'Unión Libre', 'Viudo', 'Separado', 'Divorciado'],
                    // isRequired: true,
                },

                {
                    type: 'text',
                    name: 'fecha de nacimiento',
                    title: 'fecha de nacimiento',
                    // isRequired: true,
                    inputType: 'date',
                },
                {
                    type: 'dropdown',
                    name: 'Pais',
                    title: 'Pais',
                    startWithNewLine: false,
                    choices: ['Colombia'],
                    // isRequired: true,
                },
                {
                    type: 'dropdown',
                    name: 'Departamento',
                    title: 'Departamento',
                    startWithNewLine: false,
                    choices: ['Antioqía', 'Amazonas'],
                    // isRequired: true,
                },
                {
                    type: 'dropdown',
                    name: 'Ciudad',
                    title: 'Ciudad',
                    startWithNewLine: false,
                    choices: ['Medellín'],
                    // isRequired: true,
                },

                {
                    type: 'boolean',
                    name: '¿Tiene usted equipo de cómputo y/o dispositivo móvil?',
                    title: '¿Tiene usted equipo de cómputo y/o dispositivo móvil?',
                    // isRequired: true,
                },
                {
                    type: 'boolean',
                    name: '¿Tiene usted conectividad de red (internet)?',
                    title: '¿Tiene usted conectividad de red (internet)?',
                    startWithNewLine: false,
                    // isRequired: true,
                },

                {
                    type: 'boolean',
                    name: '¿Solicitó Presupuesto Participativo?',
                    title: '¿Solicitó Presupuesto Participativo?',
                    // isRequired: true,
                },
                {
                    type: 'boolean',
                    name: 'Grupo Étnico',
                    title: 'Grupo Étnico',
                    startWithNewLine: false,
                    // isRequired: true,
                },

                {
                    type: 'boolean',
                    name: 'Pueblo indigena al que pertenece',
                    title: 'Pueblo indigena al que pertenece',
                    // isRequired: true,
                },
                {
                    type: 'boolean',
                    name: 'Comunidad negra a la que pertenece',
                    title: 'Comunidad negra a la que pertenece',
                    startWithNewLine: false,
                    // isRequired: true,
                },

                {
                    type: 'boolean',
                    name: '¿Presenta algún tipo de Discapacidad?',
                    title: '¿Presenta algún tipo de Discapacidad?',
                    // isRequired: true,
                },
                {
                    type: 'boolean',
                    name: '¿Es Desplazado o Victima de violencia?',
                    title: '¿Es Desplazado o Victima de violencia?',
                    startWithNewLine: false,
                    // isRequired: true,
                },

                {
                    type: 'boolean',
                    name: 'Tipo de Discapacidad',
                    title: 'Tipo de Discapacidad',
                    // isRequired: true,
                },
                {
                    type: 'boolean',
                    name: '¿Utiliza o tiene algún tipo de apoyo?',
                    title: '¿Utiliza o tiene algún tipo de apoyo?',
                    startWithNewLine: false,
                    // isRequired: true,
                },

                {
                    type: 'boolean',
                    name: '¿Tiene Sisben?',
                    title: '¿Tiene Sisben?',
                    // isRequired: true,
                },
                {
                    type: 'boolean',
                    name: 'Grupo de Sisben',
                    title: 'Grupo de Sisben',
                    startWithNewLine: false,
                    // isRequired: true,
                },

                {
                    type: 'boolean',
                    name: 'Correo Electrónico',
                    title: 'Correo Electrónico',
                    // isRequired: true,
                },
                {
                    type: 'boolean',
                    name: 'Confirmar correo',
                    title: 'Confirmar correo',
                    startWithNewLine: false,
                    // isRequired: true,
                },

                {
                    type: 'boolean',
                    name: '¿Te identificas como miembro de la comunidad LGBTIQ+?',
                    title: '¿Te identificas como miembro de la comunidad LGBTIQ+?',
                    // isRequired: true,
                },
                {
                    type: 'boolean',
                    name: 'Correo alternativo',
                    startWithNewLine: false,
                    title: 'Correo alternativo',
                    // isRequired: true,
                },
            ],
        },
        {
            name: 'DATOS DE RESIDENCIA',
            title: 'DATOS DE RESIDENCIA',
            elements: [
                {
                    type: 'text',
                    title: 'Dirección',
                    name: 'Dirección',
                    // isRequired: true,
                    maxLength: 25,
                    minLength: 6,
                },
                {
                    type: 'dropdown',
                    name: 'Estrato',
                    title: 'Estrato',
                    startWithNewLine: false,
                    choices: ['1', '2', '3', '4', '5', '6'],
                    // isRequired: true,
                },
                {
                    type: 'text',
                    title: 'Telefono',
                    name: 'Telefono',
                    startWithNewLine: false,
                    // isRequired: true,
                    maxLength: 25,
                    minLength: 6,
                },
                {
                    type: 'text',
                    title: 'Celular',
                    name: 'Celular',
                    startWithNewLine: false,
                    // isRequired: true,
                    maxLength: 25,
                    minLength: 6,
                },

                {
                    type: 'dropdown',
                    name: 'Ciudad',
                    title: 'Ciudad',
                    choices: ['Medellín - Antioquía', 'Abejorral - Antioquía'],
                    // isRequired: true,
                },
                {
                    type: 'dropdown',
                    name: 'Barrio',
                    title: 'Barrio',
                    startWithNewLine: false,
                    choices: ['Barrio 1', 'Barrio 2'],
                    // isRequired: true,
                },
            ],
        },
        {
            name: 'DATOS DE ESTUDIOS',
            title: 'DATOS DE ESTUDIOS',
            elements: [
                {
                    type: 'dropdown',
                    name: 'Ciudad de la Institución',
                    title: 'Ciudad de la Institución',
                    // isRequired: true,
                    choices: ['Medellín - Antioquía', 'Abejorral - Antioquía'],
                },
                {
                    type: 'dropdown',
                    title: 'Institución',
                    name: 'Institución',
                    startWithNewLine: false,
                    // isRequired: true,
                    choices: ['Colegio 1', 'Colegio 2'],
                },

                {
                    type: 'dropdown',
                    name: 'Tipo de documento con que presento las pruebas icfes',
                    title: 'Tipo de documento con que presento las pruebas icfes',
                    // isRequired: true,
                    choices: ['Medellín - Antioquía', 'Abejorral - Antioquía'],
                },
                {
                    type: 'text',
                    title: 'Numero de documento con que presento la pruebas ICFES',
                    name: 'Numero de documento con que presento la pruebas ICFES',
                    startWithNewLine: false,
                    // isRequired: true,
                    maxLength: 25,
                    minLength: 6,
                },
                {
                    type: 'dropdown',
                    title: '¿Presentó las pruebas Saber 11?',
                    name: '¿Presentó las pruebas Saber 11?',
                    startWithNewLine: false,
                    // isRequired: true,
                    choices: ['Colegio 1', 'Colegio 2'],
                },

                {
                    type: 'dropdown',
                    name: 'Año de Egreso del Colegio',
                    title: 'Año de Egreso del Colegio',
                    // isRequired: true,
                    choices: ['Medellín - Antioquía', 'Abejorral - Antioquía'],
                },
                {
                    type: 'text',
                    title: 'Código de la Pruebas Saber 11',
                    name: 'Código de la Pruebas Saber 11',
                    startWithNewLine: false,
                    // isRequired: true,
                    maxLength: 25,
                    minLength: 6,
                },
                {
                    type: 'dropdown',
                    title: 'Año de Presentación Pruebas Saber 11',
                    name: 'Año de Presentación Pruebas Saber 11',
                    startWithNewLine: false,
                    // isRequired: true,
                    choices: ['Colegio 1', 'Colegio 2'],
                },
            ],
        },
        {
            name: 'Puntaje obtenido en las pruebas Saber 11 o Examen de Estado',
            title: 'Puntaje obtenido en las pruebas Saber 11 o Examen de Estado',
            elements: [
                {
                    type: 'text',
                    title: 'Ciencias Naturales / Biología',
                    name: 'Ciencias Naturales / Biología',
                    // isRequired: true,
                    maxLength: 25,
                    minLength: 6,
                },
                {
                    type: 'text',
                    title: 'Matemáticas',
                    name: 'Matemáticas',
                    startWithNewLine: false,
                    // isRequired: true,
                    maxLength: 25,
                    minLength: 6,
                },

                {
                    type: 'text',
                    title: 'Lectura Crítica',
                    name: 'Lectura Crítica',
                    // isRequired: true,
                    maxLength: 25,
                    minLength: 6,
                },
                {
                    type: 'text',
                    title: 'Ciencias Sociales y Ciudadanas',
                    name: 'Ciencias Sociales y Ciudadanas',
                    startWithNewLine: false,
                    // isRequired: true,
                    maxLength: 25,
                    minLength: 6,
                },
                {
                    type: 'text',
                    title: 'Inglés',
                    name: 'Inglés',
                    startWithNewLine: false,
                    // isRequired: true,
                    maxLength: 25,
                    minLength: 6,
                },
            ],
        },
        {
            name: 'DATOS DE INSCRIPCIÓN PARA EL PERIODO - 2024-1',
            title: 'DATOS DE INSCRIPCIÓN PARA EL PERIODO - 2024-1',
            elements: [
                {
                    type: 'dropdown',
                    name: 'Primera Opción',
                    title: 'Primera Opción',
                    // isRequired: true,
                    choices: ['Administración', 'Ingeniería'],
                },
                {
                    type: 'dropdown',
                    name: 'Campus',
                    title: 'Campus',
                    startWithNewLine: false,
                    // isRequired: true,
                    choices: ['ROBLEDO', 'BOSTON'],
                },

                {
                    type: 'dropdown',
                    name: 'Segunda Opción',
                    title: 'Segunda Opción',
                    // isRequired: true,
                    choices: ['Administración', 'Ingeniería'],
                },
                {
                    type: 'dropdown',
                    name: 'Campus',
                    title: 'Campus',
                    startWithNewLine: false,
                    // isRequired: true,
                    choices: ['ROBLEDO', 'BOSTON'],
                },

                {
                    type: 'dropdown',
                    name: '¿Por cuál medio se enteró de las inscripciones?',
                    title: '¿Por cuál medio se enteró de las inscripciones?',
                    // isRequired: true,
                    choices: ['Publicidad', 'Ferias educativas'],
                },
            ],
        },
    ],
    completeText: 'Submit',
    showPreviewBeforeComplete: 'showAnsweredQuestions',
    showQuestionNumbers: false,
    focusFirstQuestionAutomatic: false,
    widthMode: 'static',
    width: '1000px',
};
