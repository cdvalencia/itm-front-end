import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuicklinkModule } from 'ngx-quicklink';
import { VexRoutes } from '../../../@vex/interfaces/vex-route.interface';
import { FormulariosComponent } from './formularios.component';
import { NewFormComponent } from './new-form/new-form.component';


const routes: Routes = [
    {
        path: '',
        component: FormulariosComponent
    },
    {
        path: 'new-form',
        component: NewFormComponent
    },
    {
        path: 'edit-form/:uid',
        component: NewFormComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule, QuicklinkModule]
})
export class FormulariosRoutingModule {
}
