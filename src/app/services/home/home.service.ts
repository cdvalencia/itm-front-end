import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class HomeService {
    constructor(private _http: HttpClient) {}
    getRequest(data: any) {
        let PATH = '/inicio';
        return this._http.post(`${environment.apiUrlAuth}${PATH}`, data);
    }

    sendFiles(data: any) {
        return this._http.post(`${environment.apiUrl}/Registro/SubirDocs`, data);
    }
}
