import { HttpClient } from '@angular/common/http';

import { Injectable } from '@angular/core';
import { storage } from '@lib/utils/storage/storage.utils';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    isAuthenticated$ = new BehaviorSubject<boolean>(!!storage.getItem('appSession'));

    constructor(private _http: HttpClient) {
        console.log(environment);
    }

    get isAuthenticated(): boolean {
        return this.isAuthenticated$.getValue();
    }

    autoLogin() {
        this.isAuthenticated$.next(true);
    }

    login(data: any): Observable<any> {
        let PATH = '/inicio';
        return this._http.post(`${environment.apiUrlAuth}${PATH}`, data).pipe(
            map((data: any) => {
                console.log('Login', data);                 
                if(data.idUser && !data.mustChangePass) {
                    this.isAuthenticated$.next(true);
                }
                return data;
            }),
        );
    }

    register(data: any): Observable<any> {
        let PATH = '/Registro/RegistrarUsuario';
        return this._http.post(`${environment.apiUrlAuth}${PATH}`, data).pipe(
            map((data) => {
                console.log('Register', data);
                return data;
            }),
        );
    }

    changePassword(data: any): Observable<any> {
        let PATH = '/Usuario';
        return this._http.post(`${environment.apiUrlAuth}${PATH}`, data).pipe(
            map((data) => {
                console.log('Change Password', data);
                this.isAuthenticated$.next(true);
                return data;
            }),
        );
    }

    login2(data: any): Observable<any> {
        return this._http.post(`http://localhost/saciar/controllers/historial/buscarHistorial.php`, data).pipe(
            map((data) => {
                console.log(data);
            }),
        );
        // let PATH = '/inicio';
        // return this._http.post(`${environment.apiUrlAuth}${PATH}`, data).pipe(
        //     map((data) => {
        //         console.log('Login', data);
        //         storage.setItem('appSession', { user: 'some-user-id', token: 'abc' });
        //         this.isAuthenticated$.next(true);
        //         return data;
        //     }),
        // );
    }

    logout(): void {
        storage.removeItem('appSession');
        this.isAuthenticated$.next(false);
    }
}
