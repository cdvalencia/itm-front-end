import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RequestComponent } from "./request.component";
import { TableModule } from "app/shared/table/table.module";
import { RequestRoutes } from "./request.routing";
import { PageLayoutModule } from "@vex/components/page-layout/page-layout.module";
import { MatButtonModule } from "@angular/material/button";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { MatIconModule } from "@angular/material/icon";

import { RestoreComponent } from './restore/restore.component';
import { NewPasswordComponent } from './restore/newPassword/newPassword.component';
import { NewRequestComponent } from './new-request/new-request.component';

import { InputSearchModule } from "app/shared/inputs/search/search.module";
import { MatTooltipModule } from "@angular/material/tooltip";
import { ButtonFilterModule } from "app/shared/button/button-filter/button-filter.module";
import { HeaderModule } from "app/shared/layouts/header/header.module";
import {MatSelectModule} from '@angular/material/select';
import { MatInputModule } from "@angular/material/input";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';

@NgModule({
  imports: [
    CommonModule,
    TableModule,
    RequestRoutes,
    PageLayoutModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatIconModule,
    InputSearchModule,
    MatTooltipModule,
    ButtonFilterModule,
    HeaderModule,
    MatSelectModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  declarations: [
    RequestComponent,
    RestoreComponent,
    NewPasswordComponent,
    NewRequestComponent
  ]
})
export class RequestModule {}
