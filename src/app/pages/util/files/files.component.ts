import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, ChangeDetectionStrategy, Component, OnInit, inject } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppTheme, ThemeService } from '@lib/services/theme';
import { HomeService } from "@services/home/home.service";

import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Subject } from 'rxjs';
export interface itemElement {
    id: number;
    name: string;
    type: number;
}
@Component({
    standalone: true,
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    imports: [CommonModule, RouterModule, FormsModule, MatFormFieldModule, ReactiveFormsModule, MatInputModule],
    templateUrl: './files.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilesComponent implements OnInit {
    currentTheme!: AppTheme | null;
    data: itemElement[] = [];
    dataAll: itemElement[] = [];
    filesForm: FormGroup;
    files: any = {};

    private readonly _themeService = inject(ThemeService);

    private readonly _destroy$ = new Subject();

    constructor(private fb: FormBuilder, private _home: HomeService) {
        this.filesForm = this.fb.group({
            identidad: new FormControl('', [Validators.required]),
            saber: new FormControl('', [Validators.required]),
            acta: new FormControl('', [Validators.required]),
            servicios: new FormControl('', [Validators.required]),
            sisben: new FormControl('', [Validators.required]),
            electoral: new FormControl('', [Validators.required]),
            colegio: new FormControl('', [Validators.required]),
        });
    }

    ngOnInit(): void {}

    send() {
        console.log(this.files);
        this._home.sendFiles(this.files)
          .subscribe((data: any) => {
            console.log(data);            
          });
    }

    handleUpload(event: any) {
        console.log(event);
        const file = event.target.files[0];
        const reader = new FileReader();
        reader.readAsDataURL(file);
        this.filesForm.controls[event.target.name].setValue(file.name);
        this.files[event.target.name] = {};
        this.files[event.target.name].info = file;
        reader.onload = () => {
            this.files[event.target.name].file = reader.result;
        };
    }
}
